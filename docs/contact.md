# Contacts and Support

#### Report an Issue

You can use the CMS GEM DCS JIRA project to report an issue to the DCS team: 

[click here to report issue](https://its.cern.ch/jira/secure/CreateIssue!default.jspa).

[CMS GEM DCS JIRA project](https://its.cern.ch/jira/projects/CMSGEMDCS/).

or

!!! info "Call GEM DCS Expert phone: 162109 (PIN 3337))"

#### Contact GEM DCS Experts

!!! info "Deshitha Dhammage (Developer/ Expert)"

    * [x] 📧 **Email |** [deshitha.c@cern.ch](deshitha.c@cern.ch) | [deshitha@phys.cmb.ac.lk](deshitha@phys.cmb.ac.lk)
    * [x] 🏢 **Mattermost |** [@ddhammag](https://mattermost.web.cern.ch/cms-gem-ops/messages/@ddhammag)
    * [x] 📱 **Phone |** 
	
!!! info "Ilaria Vai (Expert)"

    * [x] 📧 **Email |** [ilaria.vai@cern.ch](ilaria.vai@cern.ch)
    * [x] 🏢 **Mattermost |** [@ivai](https://mattermost.web.cern.ch/cms-gem-ops/messages/@ivai)
    * [x] 📱 **Phone |** 

