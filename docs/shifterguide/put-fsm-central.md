# Inclusion or Exclusion of GEM FSM in Central DCS

!!! info "Note" 
	**You must be the owner of the FSM to do the following steps.** If not, ask from the current owner to release FSM in central or take FSM back local for you.

!!! tip "Keep in mind" 
	<span style="color:red">**For any reason you should not keep the FSM without an owner.</span> Therefore, you must communicate with the technical shifter in CMS control room (LHC-Point5), when you are releasing the FSM in central or taking it back to local.** 
	
	
## Inclusion of GEM FSM in CMS (How to Release FSM in Central)

1. First you must inform the technical shifter (call - ) that you are going to release the FSM in central. 
2. Click on the padlock in **“GEM_Logical”** node
3. Click **“Exclude&LockOut”**

	
	![Placeholder](../assets/images/shifterguide/FSM-put-fsm-central/exclude-and-lockOut.png){ loading=lazy : style="width:420px" }
	
4. Then technical shifter can see (in central DCS screen) that you have released the FSM and he/she will include the GEM FSM in CMS (central) using the central DCS screen.

	<span style="color:blue">You can see a gray color crossed opened padlock in **“GEM_Logical”** node as following image when the node is properly **“Exclude&LockOut”**</span>

	![Placeholder](../assets/images/shifterguide/FSM-put-fsm-central/now-in-central.png){ loading=lazy : style="width:420px" }
		
5. Go inside the **“GEM_Logical”** node, you can see the red colored locked padlocks in **“GEM_Endcap_Minus”** and **“GEM_Endcap_Minus”** nodes. 
	(This means that someone else is owning these FSM nodes and here, it is the technical shifter).
	
	![Placeholder](../assets/images/shifterguide/FSM-put-fsm-central/node-in-central.png){ loading=lazy : style="width:420px" }

6. <span style="color:blue">**Now the FSM is in Central !!!**</span>
	
## Exclusion of GEM FSM from CMS (How to Take FSM in Local)

1. Inform the technical shifter (call - ) that you need to take the FSM in local and then technical shifter will release the GEM FSM from CMS using the central DCS screen. 
2. Wait until technical shifter release the GEM FSM. 
	
	<span style="color:blue">You can check this, if you go inside the **“GEM_Logical”** node, you can see the gray color opened padlocks in **“GEM_Endcap_Minus”** and **“GEM_Endcap_Minus”** nodes. </span>
	
	![Placeholder](../assets/images/shifterguide/FSM-put-fsm-central/node-released.png){ loading=lazy : style="width:420px" }
	
3. Then click on the pad lock in **“GEM_Logical”** node
	
	![Placeholder](../assets/images/shifterguide/FSM-put-fsm-central/unlockOut-and-include.png){ loading=lazy : style="width:420px" }
	
4. Click **“UnlockOut&Include”** and pad lock will become locked green color.
	
	<span style="color:blue">At the same time, the technical shifter can see that you have taken the FSM.</span>
		
5. <span style="color:blue">**Now the FSM is in Local !!!**</span>
	
!!! failure
	If you receive following message when you click **“UnlockOut&Include”**, this means still the FSM is in central and technical shifter did not release the FSM yet.

	![Placeholder](../assets/images/shifterguide/FSM-put-fsm-central/unlockOut-and-include-error.png){ loading=lazy : style="width:620px" }
	
	<span style="color:blue">**If this happens,** </span>
	
	- Click **“Dismiss”** to close the message
	- Wait until technical shifter release the FSM or ask again to release the FSM. 
	- Then click on the pad lock in **“GEM_Logical”** node, Click “Exclude&LockOut”. 
	- Then again click on the pad lock in **“GEM_Logical”** node and Click **“UnlockOut&Include”**
